import random

def get_word():
    words = ['cat', 'dog', 'elephant', 'monkey', 'snake']
    return random.choice(words)

def hangman():
    alphabet = 'abcdefghijklmnopqrstuvwxyz'
    word = get_word()
    letters_guessed = []
    lives = 5
    guessed = False

    print('The word contains', len(word), 'letters.')
    print(len(word) * '-')
    while guessed == False and lives > 0:
        print('You have ' + str(lives) + ' lives')
        guess = input('Please enter one letter').lower()
        if len(guess) == 1:
            if guess not in alphabet:
                print('You have not entered a letter.')
            elif guess in letters_guessed:
                print('You have already guessed that letter before.')
            elif guess not in word:
                print('Sorry, that letter is not part of the word :(')
                letters_guessed.append(guess)
                lives -=1
            elif guess in word:
                print('Well done, that letter exists in the word!')
                letters_guessed.append(guess)
            else:
                print('No idea')

        status = ''
        if guessed == False:
            for letter in word:
                if letter in letters_guessed:
                    status += letter
                else:
                    status += '-'
            print(status)

        if status == word:
            print('YAY! You guessed the word', word, '!!')
            guessed = True
        elif lives == 0:
            print('You died, sorry. The word was', word)

hangman()