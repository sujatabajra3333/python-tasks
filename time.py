class Time():
    def __init__(self, hour = 0, minute = 0, second = 0):
        self.hour = hour
        self.minute = minute
        self.second = second
	
def print_time(self):
    print ('(%.2d:%.2d:%.2d)' % (self.hour, self.minute, self.second))
    	
def event_time(self,seconds):
    self.second += seconds  
    if self.second >= 60:
        self.second -= 60
        self.minute += 1
        
    if self.minute >= 60:
        self.minute -= 60
        self.hour += 1

time = Time()
time.hour = 10
time.minute = 80
time.second = 0

event_time(time, 60)
print_time(time)