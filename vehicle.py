class Vehicle:
    
    def __init__(self, name, model, color,  no_of_wheels):
        self.name = name
        self.model = model
        self.color = color
        self.no_of_wheels = no_of_wheels

    def display(self):
        print ('Name: ',self.name)
        print ('Model: ',self.model)
        print ('Color: ',self.color)
        print('No of Wheels:', self.no_of_wheels)

class Bus(Vehicle):
    def __init__(self, name, model, color, no_of_wheels, passenger=0):
        Vehicle.__init__(self, name, model, color, no_of_wheels)
        self.passenger = passenger

    def addpassenger(self, no_of_passenger):
        self.passenger = self.passenger + no_of_passenger
        print(f"Current passenger: {no_of_passenger}")
        print(f"After adding passenger total number of passenger is {self.passenger}")

        if self.passenger>30:
            print(f"Seat capacity exceeded max seat is 30")
        else:
            print(f"Seats left")

class Car(Vehicle):
    def __init__(self, name, model, color, no_of_wheels, passenger=0):
        Vehicle.__init__(self, name, model, color, no_of_wheels)
        self.passenger = passenger

    def addpassenger(self, no_of_passenger):
        self.passenger =  self.passenger + no_of_passenger
        print(f"Current passenger: {no_of_passenger}")
        print(f"After adding passenger total number of passenger is {self.passenger}")
        if self.passenger>5:
            print(f"Seat capacity exceeded max seat is 5")
        else:
            print(f"Seats left")

        
obj = Bus("TATAA", 'X204', 'White', 4, 10)
obj.display()
obj.addpassenger(10)
print(obj)

obj1= Car("BMZ", '14QX', 'Red', 4, 4)
obj1.display()
obj1.addpassenger(2)
print(obj1)
